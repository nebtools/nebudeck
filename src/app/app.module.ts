import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './cmp/nav/nav.component';
import { MapComponent } from './cmp/map/map.component';
import { TreeComponent } from './cmp/tree/tree.component';
import { CRGenComponent } from './cmp/crgen/crgen.component';
import { CRMondeComponent } from './cmp/crmonde/crmonde.component';
import { OrdersComponent } from './cmp/orders/orders.component';
import { MenuComponent } from './cmp/menu/menu.component';
import { UpnbtComponent } from './cmp/upnbt/upnbt.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    MapComponent,
    TreeComponent,
    CRGenComponent,
    CRMondeComponent,
    OrdersComponent,
    MenuComponent,
    UpnbtComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
