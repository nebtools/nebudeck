import { Monde, IMondes } from './monde';
import { Joueur, IJoueurs } from './joueur';
import { Flotte, IFlottes } from './flotte';
import { Tresor, ITresors } from './tresor';
export class Partie{
	tours:ITreeTours={};
	constructor(public Gen:General){}
}

export class TreeTour{
	constructor(
		public path:string,
		public gen:General,
		public tech:Techs,
		public j:IJoueurs,
		public m:IMondes,
		public f:IFlottes,
		public t:ITresors){}
}

export interface ITreeTours{
	[index:string]:TreeTour;
}

export class Techs{
	MaxDEP:number[];
	MaxATT:number[];
	MaxDEF:number[];
	MaxRAD:number[];
	MaxALI:number[];
	MaxCAR:number[];
	DEPClasse:number[];
	ATTClasse:number[];
	DEFClasse:number[];
	RADClasse:number[];
	ALIClasse:number[];
	CARClasse:number[];
}
export class Tech{
	constructor(
		public Lvl:number,
		public Cost:number,
		public Prog:number,
		public Max:number){}
}
export class General{
	NomPartie : string;
	Tour: number;
	NoJoueur:number;
	NbMondes:number;
	NbFlottes:number;
	NbJoueurs:number;
	SizeX:number;
	SizeY:number;
}