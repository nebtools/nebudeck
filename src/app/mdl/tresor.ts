export class Tresor {
  Localisation:number;
  Status:number;
  Proprio:number;
  Nom:string;
  constructor(public id:number){}
}
export interface ITresors{
  [index:number]:Tresor;
}
