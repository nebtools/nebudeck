export class SVGTools{
  static newSVGTag(name:string):SVGElement{
    return document.createElementNS('http://www.w3.org/2000/svg',name);
  }
  static addSVGTag(parent:SVGElement, name:string):SVGElement{
    let t=SVGTools.newSVGTag(name);
    parent.appendChild(t);
    return t;
  }
}
