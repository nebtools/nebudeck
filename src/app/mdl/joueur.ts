export class Joueur {
  Pseudo:string;
  Nom:string;
  Classe:number;
  TourChgtJihad:number;
  Jihad:number;
  DEP:number;
  ATT:number;
  DEF:number;
  RAD:number;
  ALI:number;
  CAR:number;
  DEPReste:number;
  ATTReste:number;
  DEFReste:number;
  RADReste:number;
  ALIReste:number;
  CARReste:number;
  CA:number;
  CD:number;
  Score:number;
  Email:string;
  diplo:IDiplo={};
  DetailScore:number[];
  constructor(public id:number){}
}
export class TourJoueur {

}
export interface IJoueurs{
  [index:number]:Joueur;
}
export interface ITourJoueurs{
  [index:number]:TourJoueur;
}
export class Diplo{
  Connu:boolean;
  Contact:boolean;
  Allie:boolean;
  Chargeur:boolean;
  ChargeurPop:boolean;
  DechargeurPop:boolean;
  Pilleur:boolean;
}
export interface IDiplo{
  [index:number]:Diplo;
}