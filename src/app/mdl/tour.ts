import { Partie } from './partie';
import { ITourJoueurs } from './joueur';
import { IMondes } from './monde';
import { IFlottes } from './flotte';

export class Tour{
  constructor(public partie:Partie,public num:number,public joueurs:ITourJoueurs, public mondes:IMondes, public flottes:IFlottes){

  }
}