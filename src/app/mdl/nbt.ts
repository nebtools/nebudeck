import { General, Techs, Tech,TreeTour } from './partie';
import { Monde, IMondes } from './monde';
import { Joueur, IJoueurs, Diplo } from './joueur';
import { Flotte, IFlottes } from './flotte';
import { Tresor, ITresors } from './tresor';

export class NBT{
  static parseTour(strnbt:string):TreeTour{
    let lines=strnbt.split("\n");
    let state=0;
    let id=0;
    let gen= new General();
    let techs= new Techs();
    let joueurs:IJoueurs={};
    let mondes:IMondes={};
    let flottes:IFlottes={};
    let tresors:ITresors={};
    for(let ll of lines){
      let l=ll.trim();
      if(l.startsWith('[')){
        console.log("\""+l+"\"");
        switch(l){
        case "[GENERAL]":
          state=1;
          console.log("1");
          break;
        case "[TECHNO]":
          state=2;
          console.log("2");
          break;
        default:
          switch(l.substring(1,3)){
          case "J_":
            state=3;
            console.log(l);
            id=parseInt(l.substring(3,l.length-1));
            joueurs[id]=new Joueur(id);
            break;
/*          case "Md_":
          case "Md#":
            state=4;
            console.log(l);
            id=parseInt(l.substring(4,l.length-1));
            mondes[id]=new Monde(id);
            if(l[3]=='#')
              mondes[id].Bombe=true;
            break;
          case "M#":*/
          case "M_":
            state=4;
            console.log(l);
            id=parseInt(l.substring(3,l.length-1));
            mondes[id]=new Monde(id);
/*            if(l[2]=='#')
              mondes[id].Bombe=true;*/
            break;
//          case "F#":
          case "F_":
            state=5;
            console.log(l);
            id=parseInt(l.substring(3,l.length-1));
            flottes[id]=new Flotte(id);
            break;
          case "T_":
            state=6;
            console.log(l);
            id=parseInt(l.substring(3,l.length-1));
            tresors[id]=new Tresor(id);
            break;
          }
        }
      }else{
        let p=l.split("=",2);
        if(p.length==2)
          switch (state){
          case 1: // GENERAL
            switch(p[0]){
              case "NomPartie":
                gen.NomPartie=p[1];
                break;
              case "Tour":
                gen.Tour=parseInt(p[1]);
                break;
              case "NoJoueur":
                gen.NoJoueur=parseInt(p[1]);
                break;
              case "NbMondes":
                gen.NbMondes=parseInt(p[1]);
                break;
              case "NbFlottes":
                gen.NbFlottes=parseInt(p[1]);
                break;
              case "NbJoueurs":
                gen.NbJoueurs=parseInt(p[1]);
                break;
              case "SizeX":
                gen.SizeX=parseInt(p[1]);
                break;
              case "SizeY":
                gen.SizeY=parseInt(p[1]);
                break;
            }
            break;
          case 2: // TECHNO
            let ps=p[1].split(',').map(s=>parseInt(s));
            techs[p[0]]=ps;
            break;
          case 3: // JOUEUR
            switch(p[0]){
            case "Pseudo":
              joueurs[id].Pseudo=p[1];
              break;
            case "Nom":
              joueurs[id].Nom=p[1];
              break;
            case "J":
              let ps=p[1].split(',');
              joueurs[id].Classe=parseInt(ps[0]);
              joueurs[id].TourChgtJihad=parseInt(ps[1]);
              joueurs[id].Jihad=parseInt(ps[2]);
              joueurs[id].DEP=parseInt(ps[3]);
              joueurs[id].ATT=parseInt(ps[4]);
              joueurs[id].DEF=parseInt(ps[5]);
              joueurs[id].RAD=parseInt(ps[6]);
              joueurs[id].ALI=parseInt(ps[7]);
              joueurs[id].CAR=parseInt(ps[8]);
              joueurs[id].DEPReste=parseInt(ps[9]);
              joueurs[id].ATTReste=parseInt(ps[10]);
              joueurs[id].DEFReste=parseInt(ps[11]);
              joueurs[id].RADReste=parseInt(ps[12]);
              joueurs[id].ALIReste=parseInt(ps[13]);
              joueurs[id].CARReste=parseInt(ps[14]);
              joueurs[id].CA=parseInt(ps[15]);
              joueurs[id].CD=parseInt(ps[16]);
              joueurs[id].Score=parseInt(ps[17]);
              break;
            case "Email":
              joueurs[id].Email=p[1];
              break;
            case "ConNom":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(ps)).Connu=true;
              break;
            case "Contact":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(ps)).Contact=true;
              break;
            case "Allies":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(ps)).Allie=true;
              break;
            case "Chargeurs":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(ps)).Chargeur=true;
              break;
            case "ChargeursPop":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(p[1])).ChargeurPop=true;
              break;
            case "DechargeursPop":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(p[1])).DechargeurPop=true;
              break;
            case "Pilleurs":
              for(let ps of p[1].split(','))
                this.getDiplo(joueurs[id],parseInt(p[1])).Pilleur=true;
              break;
            case "DetailScore":
              joueurs[id].DetailScore=p[1].split(',').map(s=>parseInt(s));
              break;
            }
            break;
          case 4: // MONDE
            switch(p[0]){
            case "Coord":
              let crds=p[1].split(',').map(s=>parseInt(s));
              if(crds.length==2)
                mondes[id].Coord=crds;
              break;
            case "C":
              let cn=p[1].split(',').map(s=>parseInt(s));
              mondes[id].cn=cn;
              break;
            case "M":
              let m=p[1].split(',').map(s=>parseInt(s));
              mondes[id].Duree=m[0];
              mondes[id].ProprConv=m[1];
              mondes[id].PlusMP=m[2];
              mondes[id].PC=m[3];
              mondes[id].Proprio=m[4];
              mondes[id].Ind=m[5];
              mondes[id].Pop=m[6];
              mondes[id].Conv=m[7];
              mondes[id].Robot=m[8];
              mondes[id].MaxPop=m[9];
              mondes[id].MP=m[10];
              mondes[id].VI=m[11];
              mondes[id].VP=m[12];
              mondes[id].Pi=m[13];
              mondes[id].RecupPi=m[14];
              mondes[id].AncienProprio=m[15];
              mondes[id].IndLibre=m[16];
              mondes[id].PopLibre=m[17];
              mondes[id].Capture=m[18]>0;
              mondes[id].Cadeau=m[19]>0;
              mondes[id].TN=m[20]>0;
              mondes[id].Bombe=m[21]>0;
              mondes[id].Pille=m[22]>0;
              mondes[id].PremierProprioJihad=m[23]>0;
              break;
            case "ME":
              let me=p[1].split(',').map(s=>parseInt(s));
              mondes[id].ActionVI=me[0];
              mondes[id].ActionVP=me[1];
              mondes[id].CibleVI=me[2];
              mondes[id].CibleVP=me[3];
              mondes[id].PopMoins=me[4];
              mondes[id].ConvMoins=me[5];
              mondes[id].RobMoins=me[6];
              mondes[id].PC=me[7];
              break;
            case "Explo":
              let ex=p[1].split(',').map(s=>parseInt(s));
              mondes[id].NbExplo=ex[0];
              mondes[id].NbExploCM=ex[1];
              mondes[id].NbExploPop=ex[2];
              mondes[id].NbExploLoc=ex[3];
              mondes[id].Localise=ex[4];
              //mondes[id].StationMonde=ex[5];
              //mondes[id].Connu=ex[6];
              mondes[id].PotExplo=ex[7];
              break;
            case "Nom":
              mondes[id].Nom=p[1];
              break;
            }
            break;
          case 5: // FLOTTE
            switch(p[0]){
              case "F":
                let f=p[1].split(',').map(s=>parseInt(s));
                flottes[id].Localisation=f[0];
                flottes[id].NbVC=f[1];
                flottes[id].NbVT=f[2];
                flottes[id].MP=f[3];
                flottes[id].N=f[4];
                flottes[id].C=f[5];
                flottes[id].R=f[6];
                flottes[id].Proprio=f[7];
                flottes[id].AncienProprio=f[8];
                flottes[id].OrdrExcl=f[9];
                flottes[id].Cible=f[10];
                flottes[id].Paix=f[11]>0;
                flottes[id].TransporteBombe=f[12]>0;
                flottes[id].Capturee=f[13]>0;
                flottes[id].Piratee=f[14]>0;
                flottes[id].Offerte=f[15]>0;
                flottes[id].FlotteEmbuscade=f[16]>0;
                flottes[id].DPC=f[17]>0;
                flottes[id].FlotteExplo=f[18]>0;
                break;
              case "Chemin":
                let ch=p[1].split(',').map(s=>parseInt(s));
                flottes[id].ch=ch;
                break;
            }
            break;
          case 6: // TRESOR
            switch(p[0]){
            case "T":
              let t=p[1].split(',');
              tresors[id].Localisation=parseInt(t[2]);
              tresors[id].Status=t[1]=='M'?0:1;
              tresors[id].Proprio=parseInt(t[0]);
              break;
              case "Nom":
                tresors[id].Nom=p[1];
              break;
            }
            break;
          }
      }
    }
    return new TreeTour('/'+gen.Tour,gen,techs,joueurs,mondes,flottes,tresors);
  }
  static getDiplo(j:Joueur,id:number):Diplo{
    if(!j.diplo[id])
      j.diplo[id]=new Diplo();
    return j.diplo[id];
  }
}