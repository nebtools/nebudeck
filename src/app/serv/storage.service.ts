import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  getObject(path:string):any{
    return JSON.parse(localStorage.getItem(path));
  }
  setObject(path:string,obj:any){
    console.log('set',path,obj);
    localStorage.setItem(path,JSON.stringify(obj));
  }

}
