import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { BehaviorSubject } from 'rxjs';
import { Tour } from '../mdl/tour';
import { Partie,TreeTour,General } from '../mdl/partie';

@Injectable({
  providedIn: 'root'
})
export class NebulaService {

  private partieSource = new BehaviorSubject({});
  private tourSource = new BehaviorSubject({});
  private mondeSource = new BehaviorSubject(0);
  public currentTour=this.tourSource.asObservable();
  public currentPartie=this.partieSource.asObservable();
  private partie:Partie;
  constructor(private storage:StorageService) { 
    let l=this.listParties();
    if(l && l.length>0)
      this.switchPartie(l[0]);
  }

  listParties():string[]{
    let l=this.storage.getObject('nebudeck_partie');
    if(l) return l;
    else return [];
  }
  switchPartie(name:string){
    if(this.partie &&this.partie.Gen && this.partie.Gen.NomPartie==name)
      return;
    this.partie=new Partie(<General>this.storage.getObject('nebudeck_gen_'+name));
    let tours=<string[]>this.storage.getObject('nebudeck_tours_'+name);
    tours.sort();
    let lastp;
    for(let p in tours){
      this.partie.tours[p]=<TreeTour>this.storage.getObject('nebudeck_tour_'+name+p);
      lastp=p;
    }
    this.partieSource.next(this.partie);
    this.tourSource.next(lastp);
  }
  addTour(tour:TreeTour){
    let parties=this.listParties();
    if(parties.indexOf(tour.gen.NomPartie)<0){
      parties.push(tour.gen.NomPartie);
      this.storage.setObject('nebudeck_partie',parties);
      this.partie=new Partie(tour.gen);
      this.partie.tours[tour.path]=tour;
    }
    this.saveTour(tour);
    this.switchPartie(tour.gen.NomPartie);
    this.setTour(tour);
  }
  saveTour(tour:TreeTour){
    this.storage.setObject('nebudeck_tour_'+tour.gen.NomPartie+'_'+tour.path,tour);
  }
  setTour(t:TreeTour){
    this.tourSource.next(t.path);
  }
  setMonde(n:number){
    this.mondeSource.next(n);
  }
  getPartie():Partie{
    return this.partie;
  }
}
