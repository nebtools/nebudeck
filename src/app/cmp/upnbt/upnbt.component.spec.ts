import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpnbtComponent } from './upnbt.component';

describe('UpnbtComponent', () => {
  let component: UpnbtComponent;
  let fixture: ComponentFixture<UpnbtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpnbtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpnbtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
