import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NBT } from '../../mdl/nbt';
import { NebulaService } from '../../serv/nebula.service';

@Component({
  selector: 'app-upnbt',
  templateUrl: './upnbt.component.html',
  styleUrls: ['./upnbt.component.css']
})
export class UpnbtComponent implements OnInit {

  @Output() closeEvent=new EventEmitter();
  nbtFile:File;

  constructor(private neb:NebulaService) { }

  ngOnInit(): void {
  }
  close(){
    this.closeEvent.emit();
  }
  addNbt(){
    console.log(this.nbtFile);
    if(this.nbtFile){
      var r=new FileReader();
      r.readAsText(this.nbtFile,'ISO-8859-1');
      r.onload=this.loadNbt.bind(this);
    }
    this.close();
  }
  loadNbt(e:ProgressEvent<FileReader>){
    let tour=NBT.parseTour(<string>e.target.result);
    console.log(tour);
    this.neb.addTour(tour);
  }
  changeFile(e:Event){
    console.log(e);
    this.nbtFile=(<HTMLInputElement>e.target).files[0];
  }
}
