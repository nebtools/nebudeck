import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CRGenComponent } from './crgen.component';

describe('CRGenComponent', () => {
  let component: CRGenComponent;
  let fixture: ComponentFixture<CRGenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CRGenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CRGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
