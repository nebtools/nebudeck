import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CRMondeComponent } from './crmonde.component';

describe('CRMondeComponent', () => {
  let component: CRMondeComponent;
  let fixture: ComponentFixture<CRMondeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CRMondeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CRMondeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
