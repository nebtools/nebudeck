import { Component, OnInit, ViewChild,ElementRef,EventEmitter } from '@angular/core';
import { NebulaService } from '../../serv/nebula.service';
import { TreeTour } from '../../mdl/partie';
import { ILink } from '../../mdl/ilink';
import { INode } from '../../mdl/inode';
import * as d3 from 'd3';
const FORCES = {
  LINKS: 1 / 50,
  COLLISION: 1,
  CHARGE: -1
}
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  public ticker: EventEmitter<d3.Simulation<Node, Link>> = new EventEmitter();
  public simulation: d3.Simulation<any, any>;

  public nodes: INode[] = [];
  public links: ILink[] = [];

 @ViewChild('map') svg:ElementRef<SVGSVGElement>;
 
  constructor(private neb:NebulaService) { }

  ngOnInit(): void {
    this.neb.currentTour.subscribe(this.changeTour);
    this.neb.getPartieTour.subscribe(
      data=>{
        console.log(data);
        this.buildSim();
      });
  }
  changeTour(tour:TreeTour){
    console.log(tour);
  }

  buildSim(){
    let ticker=this.ticker;
    this.simulation=new d3.forceSimulation()
      .force('charge',
        d3.forceManyBody().strength(d => FORCES.CHARGE * d['r']))
      .force('collide',
        d3.forceCollide().strength(FORCES.COLLISION).radius(d => d['r']  +5).iterations(2));
    this.simulation.on('tick',function(){ ticker.emit(this)});
    this.simulation.nodes(this.nodes);
    this.simulation.force('link',d3.forceLink(this.links)
      .id(d => d['id'])
      .strength(FORCES.LINKS));
  }
}
