import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { NebulaService } from '../../serv/nebula.service';
import { TreeTour,General,Techs } from '../../mdl/partie';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @ViewChild('map') svg:ElementRef<HTMLDivElement>;
  opened=false;
  uplwin=false;
  constructor(private neb:NebulaService) { }

  ngOnInit(): void {
  }

  openMenu(){
    let t=new TreeTour('main',new General(),new Techs(),{},{},{},{});
    this.neb.setTour(t);
  	this.opened=true;
  }

  openUpl(){
    this.uplwin=true;
  }
  closeUpl(){
    this.uplwin=false;
  }
}
