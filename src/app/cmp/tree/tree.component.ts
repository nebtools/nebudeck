import { Component, AfterViewInit, ViewChild,ElementRef } from '@angular/core';
import { NebulaService } from '../../serv/nebula.service';
import { Partie,TreeTour } from '../../mdl/partie';
import { SVGTools } from '../../mdl/svgtools';
@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements AfterViewInit {

 @ViewChild('tree') svg:ElementRef<SVGSVGElement>;

  name='';
  partie:Partie;
  constructor(private neb:NebulaService) { }

  ngAfterViewInit(): void {
    this.partie= this.neb.getPartie();
    this.neb.currentTour.subscribe(this.changeTour.bind(this));
    this.neb.currentPartie.subscribe(this.changePartie.bind(this));
    this.showTree();
  }
  changePartie(partie:Partie){
    this.partie=partie;
    this.showTree();
  }
  changeTour(path:string){
  }

  showTree(){
    this.svg.nativeElement.innerHTML="";
    let x=10;
    let y=5;
    let dec:number[]=[];
    for(const p in this.partie.tours){
      let ps=p.split('/');
      
    }
  }
  setTour(e:MouseEvent){
    let b=(<SVGElement>e.target).getAttribute('b');
    let n=(<SVGElement>e.target).getAttribute('n');
  }
  showNode(node:TreeTour,x:number,y:number):number{
    let size=10;
    let n=SVGTools.addSVGTag(this.svg.nativeElement,'path');
    n.setAttribute("d","M "+x+","+y+" l 0 20");
    n.classList.add('treepath');
    let c=SVGTools.addSVGTag(this.svg.nativeElement,'circle');
    c.setAttribute('cx',''+x);
    c.setAttribute('cy',''+(y+10));
    c.setAttribute('b',''+node.branch);
    c.setAttribute('n',''+node.gen.Tour);
    c.addEventListener('click',this.setTour.bind(this));
    c.classList.add('treecircle');
    let t=SVGTools.addSVGTag(this.svg.nativeElement,'text');
    t.innerHTML=''+node.gen.Tour;
    t.classList.add('treetext');
    t.setAttribute('x',''+x);
    t.setAttribute('y',''+(y+13));
    t.setAttribute('b',''+node.branch);
    t.setAttribute('n',''+node.gen.Tour);
    t.addEventListener('click',this.setTour.bind(this));
    let dec=0;
    for(let nb in node.next){
      size=Math.max(size,this.showNode(node.next[nb],x+(20*dec++),y+20));
    }
    return size;
  }
}
