export class General {
	NomPartie : string;
	Tour: number;
	NoJoueur:number;
	NbMondes:number;
	NbFlottes:number;
	NbJoueurs:number;
	SizeX:number;
	SizeY:number;
	props:any={};
	constructor(jgen:any){
		this.NomPartie=jgen.NomPartie;
		this.Tour=parseInt(jgen.Tour);
		this.NoJoueur=parseInt(jgen.NoJoueur);
		this.NbMondes=parseInt(jgen.NbMondes);
		this.NbFlottes=parseInt(jgen.NbFlottes);
		this.NbJoueurs=parseInt(jgen.NbJou);
		this.SizeX=parseInt(jgen.TaillePlanX);
		this.SizeY=parseInt(jgen.TaillePlanY);
		for(let kg in jgen){
			switch(kg){
			case "NomPartie":
			case "Tour":
			case "NoJoueur":
			case "NbMondes":
			case "NbFlottes":
			case "NbJou":
			case "TaillePlanX":
			case "TaillePlanY":
				break;
			default:
			this.props[kg]=jgen[kg];
			}
		}
	}
	toJson(){
		let jgen:any={};
		jgen.NomPartie=this.NomPartie;
		jgen.Tour=this.Tour;
		jgen.NoJoueur=this.NoJoueur;
		jgen.NbMondes=this.NbMondes;
		jgen.NbFlottes=this.NbFlottes;
		jgen.NbJou=this.NbJoueurs;
		jgen.TaillePlanX=this.SizeX;
		jgen.TaillePlanY=this.SizeY;
		return jgen;
	}
	toNBT():string{
		let str="";
		str+="NomPartie="+this.NomPartie+"\n";
		str+="Tour="+this.Tour+"\n";
		str+="NoJoueur="+this.NoJoueur+"\n";
		str+="NbMondes="+this.NbMondes+"\n";
		str+="NbFlottes="+this.NbFlottes+"\n";
		str+="NbJou="+this.NbJoueurs+"\n";
		str+="TaillePlanX="+this.SizeX+"\n";
		str+="TaillePlanY="+this.SizeY+"\n";
		for(let kg in this.props){
			str+=kg+"="+this.props[kg]+"\n";
		}
		return str;
	}
}