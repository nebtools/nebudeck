export class Partie {
	Tour:number;
	Nom:string;
	m:ISMondes={};
	SizeX:number;
	SizeY:number;
	constructor(jp:any){
		if(jp){
			this.Tour=jp.Tour;
			this.Nom=jp.Nom;
			this.SizeX=jp.SizeX;
			this.SizeY=jp.SizeY;
			for(let km in jp.m){
				if(km=='M_116')
					console.log(jp.m[km]);
				this.m[km]=new SMonde(km,jp.m[km]);
			}
		}
		else
			this.Tour=0;
	}
	toJson(){
		let jp:any={};
		jp.Tour=this.Tour;
		jp.Nom=this.Nom;
		jp.m={}
		for(let km in this.m){
			jp.m[km]=this.m[km].toJson();
		}
		jp.SizeX=this.SizeX;
		jp.SizeY=this.SizeY;
		return jp;
	}
}

export class SMonde {
	id:string;
	x:number;
	y:number;
	cn:number[];
	t:any;
	comment:string;
	constructor(id:string,jm:any){
		this.id=id;
		if(id=='M_116')
			console.log(jm);
		if(jm){
			this.x=parseInt(jm.x);
			this.y=parseInt(jm.y);
			this.comment=jm.cm;
			this.cn=[];
			if(jm.cn)
				for(let c of jm.cn){
					this.cn.push(parseInt(c));
				}
		}
		if(id=='M_116')
			console.log(this);
	}
	toJson(){
		let jm:any={};
		jm.x=this.x;
		jm.y=this.y;
		jm.cn=this.cn;
		jm.cm=this.comment;
		return jm;
	}
}
export class SMTour {

}
export interface ISMondes {
	[index:string]:SMonde;
}

export interface ISMTour {
	[index:string]:SMTour;
}