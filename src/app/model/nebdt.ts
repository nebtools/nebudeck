export class NebDt {
	parseInt(s:string):number{
		if(s && s.length>0)
			return parseInt(s);
		else
			return 0;
	}
	parseBool(s:string):boolean{
		if(s&&s.length>0&&parseInt(s)>0)
			return true;
		else
			return false;
	}
}
