import {Tour} from './tour';
import {NebDt}from './nebdt';

export class Flotte extends NebDt {
	id:string;
	Localisation:number;
	NbVC:number;
	NbVT:number;
	MP:number;
	N:number;
	C:number;
	R:number;
	Proprio:number;
	AncienProprio:number;
	OrdrExcl:number;
	Cible:number;
	Paix:boolean;
	TransporteBombe:boolean;
	Capturee:boolean;
	Piratee:boolean;
	Offerte:boolean;
	FlotteEmbuscade:boolean;
	DPC:boolean;
	FlotteExplo:boolean;
	ch:number[];
	tour:Tour;
	constructor(id:string,tour:Tour,jf:any){
		super();
		this.id=id;
		this.tour=tour;
		if(jf.F){
			let ff=jf.F.split(',');
			this.Localisation = this.parseInt(ff[0]);
			this.NbVC = this.parseInt(ff[1]);
			this.NbVT = this.parseInt(ff[2]);
			this.MP = this.parseInt(ff[3]);
			this.N = this.parseInt(ff[4]);
			this.C = this.parseInt(ff[5]);
			this.R = this.parseInt(ff[6]);
			this.Proprio = this.parseInt(ff[7]);
			this.AncienProprio = this.parseInt(ff[8]);
			this.OrdrExcl = this.parseInt(ff[9]);
			this.Cible = this.parseInt(ff[10]);
			this.Paix = this.parseInt(ff[11])>0;
			this.TransporteBombe = this.parseBool(ff[12]);
			this.Capturee = this.parseBool(ff[13]);
			this.Piratee = this.parseBool(ff[14]);
			this.Offerte = this.parseBool(ff[15]);
			this.FlotteEmbuscade = this.parseBool(ff[16]);
			this.DPC = this.parseBool(ff[17]);
			this.FlotteExplo = this.parseBool(ff[18]);
		}
		this.ch=[];
		if(jf.Chemin!==undefined){
			if(jf.Chemin.length>0){
				let ch=jf.Chemin.split(",");
				for(let m of ch)
					this.ch.push(this.parseInt(m));
			}
		}
	}
	toJson(){
		let jf:any={};
		let jfF:any=[];
		jfF.push(this.Localisation);
		jfF.push(this.NbVC);
		jfF.push(this.NbVT);
		jfF.push(this.MP);
		jfF.push(this.N);
		jfF.push(this.C);
		jfF.push(this.R);
		jfF.push(this.Proprio);
		jfF.push(this.AncienProprio);
		jfF.push(this.OrdrExcl);
		jfF.push(this.Cible?1:0);
		jfF.push(this.Paix?1:0);
		jfF.push(this.TransporteBombe?1:0);
		jfF.push(this.Capturee?1:0);
		jfF.push(this.Piratee?1:0);
		jfF.push(this.Offerte?1:0);
		jfF.push(this.FlotteEmbuscade?1:0);
		jfF.push(this.DPC?1:0);
		jfF.push(this.FlotteExplo?1:0);
		jf.F=jfF.join(',');
		if(this.ch.length>0){
			let jfChemin=[];
			for(let m of this.ch)
				jfChemin.push(m);
			jf.Chemin=jfChemin.join(',');
		}
		return jf;
	}
	addOrNot(val:number){
		if(val>0)
			return ""+val;
		return "";
	}
	toNBT():string{
		let str="F=";
		str+=this.addOrNot(this.Localisation)+',';
		str+=this.addOrNot(this.NbVC)+',';
		str+=this.addOrNot(this.NbVT)+',';
		str+=this.addOrNot(this.MP)+',';
		str+=this.addOrNot(this.N)+',';
		str+=this.addOrNot(this.C)+',';
		str+=this.addOrNot(this.R)+',';
		str+=this.addOrNot(this.Proprio)+',';
		str+=this.addOrNot(this.AncienProprio)+',';
		str+=this.addOrNot(this.OrdrExcl)+',';
		str+=this.addOrNot(this.Cible?1:0)+',';
		str+=this.addOrNot(this.Paix?1:0)+',';
		str+=this.addOrNot(this.TransporteBombe?1:0)+',';
		str+=this.addOrNot(this.Capturee?1:0)+',';
		str+=this.addOrNot(this.Piratee?1:0)+',';
		str+=this.addOrNot(this.Offerte?1:0)+',';
		str+=this.addOrNot(this.FlotteEmbuscade?1:0)+',';
		str+=this.addOrNot(this.DPC?1:0)+',';
		str+=this.addOrNot(this.FlotteExplo?1:0)+"\n";
		if(this.ch.length>0){
			str+="Chemin="+this.ch[0];
			for(let i=1;i<this.ch.length;i++)
				str+=","+this.ch[i];
			str+="\n";
		}
		return str;
	}
	getCR():string{
		let str='';
		if(this.TransporteBombe)
			str+="F#"+this.id.substr(2);
		else
			str+=this.id;
		str+="\t";
		if(this.Proprio>0){
			if(this.Paix)
				str+='(';
			str+='"'+this.tour.j['J_'+this.Proprio].Pseudo;
			if(this.AncienProprio>0&&this.AncienProprio!=this.Proprio)
				str+='('+this.tour.j['J_'+this.AncienProprio].Pseudo+')';
			str+='"';
			if(this.Paix)
				str+=')';
			if(this.Capturee)
				str+='!';
			if(this.Piratee)
				str+='P';
			if(this.Offerte)
				str+='C';
			if(this.DPC)
				str+='D';
			if(this.FlotteExplo)
				str+='E';
			if(this.NbVC>0 ||this.NbVT>0 ||(this.OrdrExcl>0&&this.OrdrExcl<7)){
				str+='[';
				if(this.tour.g.NoJoueur!=this.Proprio)
					str+='?';
				else{
					let virg=false;
					if(this.MP>0){
						str+=this.MP;
						virg=true;
					}
					if(this.N>0){
						if(virg) str+=','; else virg=true;
						str+=this.N+'N';
					}
					if(this.C>0){
						if(virg) str+=','; else virg=true;
						str+=this.C+'C';
					}
					if(this.R>0){
						if(virg) str+=',';
						str+=this.R+'R';
					}
				}
				str+=']';
				if(this.NbVC>0||this.NbVT>0){
					str+='=';
					let virg=false;
					if(this.NbVC>0){
						str+=this.NbVC;
						virg=true;
					}
					if(this.NbVT>0){
						if(virg) str+='+';
						str+=this.NbVT+'T'
					}
					if(this.Proprio!=this.tour.g.NoJoueur&&this.tour.j['J_'+this.tour.g.NoJoueur].ALI==0)
						str+='?';
				}
				switch(this.OrdrExcl){
				case 1:
					str+="* F_"+this.Cible;
					break;
				case 2:
					str+="* M";
					break;
				case 3:
					str+="* P";
					break;
				case 4:
					str+="* I";
					break;
				case 5:
					str+="* R";
					break;
				case 6:
					str+="* B";
					break;
				}
				if(this.FlotteEmbuscade)
					str+="**";
			}
		}
		if(this.ch){
			str +=' du M_';
			if(this.ch.length==0)
				str+='?';
			else
				str+=this.ch[this.ch.length-1];
		}
		return str;
	}
	getTrace(id:number):string{
		let str='';
		let i=0;
		for(;i<this.ch.length;i++)
			if(this.ch[i]==id)
				break;
		if(i>=this.ch.length)
			return '';
		str+='{'+this.id+'\t"'+this.tour.j['J_'+this.AncienProprio].Pseudo+'"';
		if(i>0)
			str+=' du M_'+this.ch[i-1];
		if(i<this.ch.length-1)
			str+=' vers M_'+this.ch[i+1];
		else if(this.Localisation)
			str+=' vers M_'+this.Localisation;
		else
			str+=' vers M_?';
		str+='}';
		return str;
	}
}

export interface IFlottes{
	[index:string]:Flotte;
}